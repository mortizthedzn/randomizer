package main

import (
	"flag"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var (
	Token string
	owner = "325682674482020352"
)

func init() {

	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		log.Fatalln("Error creating Discord session,", err)
		return
	}

	dg.AddHandler(messageCreate)

	dg.Identify.Intents = discordgo.IntentsGuildMessages
	err = dg.Open()
	if err != nil {
		log.Fatalln("Error opening connection,", err, "\nMake sure you have input your token with -t <token>")
		return
	}

	fmt.Println("Randomizer is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}


func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}
	
	if m.Content == "r.dado" {
		resp, err := http.Get("https://www.random.org/integers/?num=1&min=1&max=20&col=5&base=10&format=plain&rnd=new")
		if err != nil {
			log.Fatalln(err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}
		sb := string(body)
		log.Println(m.Author.ID + " used dice command and got: " + sb)
		s.ChannelMessageSend(m.ChannelID, "🎲 | O dado rolou e você conseguiu... " + sb)
	}
	if m.Content == "r.roll" {
		resp, err := http.Get("https://www.random.org/integers/?num=1&min=1&max=20&col=5&base=10&format=plain&rnd=new")
		if err != nil {
			log.Fatalln(err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}
		sb := string(body)
		log.Println(m.Author.ID + " used dice command and got: " + sb)
		s.ChannelMessageSend(m.ChannelID, "🎲 | O dado rolou e você conseguiu... " + sb)
	}

	if m.Content == "r.stop" {
		if m.Author.ID == owner {
		log.Println("Owner sent stop request.")
		s.ChannelMessageSend(m.ChannelID, "Adeus!")
		s.Close()
		os.Exit(0)
		} else {
		log.Println(m.Author.ID + " tried to stop the bot!")
		return
		}
	}
	if m.Content == "r.help" {
	s.ChannelMessageSend(m.ChannelID, "**Meus comandos:**\n\u0060r.dado\u0060: Rola um dado usando o random.org\n\u0060r.ajuda\u0060: Esse comando, exibe os comandos do bot.\n\u0060r.stop\u0060: Para o bot.")
	log.Println(m.Author.ID + " used help command")
	}
	if m.Content == "r.ajuda" {
	s.ChannelMessageSend(m.ChannelID, "**Meus comandos:**\n\u0060r.dado\u0060: Rola um dado usando o random.org\n\u0060r.ajuda\u0060: Esse comando, exibe os comandos do bot.\n\u0060r.stop\u0060: Para o bot.")
	log.Println(m.Author.ID + " used help command")
	}
	if m.Content == "r.invite" {
	s.ChannelMessageSend(m.ChannelID, "https://discord.com/api/oauth2/authorize?client_id=875798149837488129&permissions=0&scope=bot")
	
	}
}
